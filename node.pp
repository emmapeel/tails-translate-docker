# translate.tails.boum.org
node 'default' {

  # If no "path" is set, puppet apply fails because "find" (from
  # puppet-apt/manifests/config.php) is not fully qualified.
  Exec {
    path => '/bin:/sbin:/usr/bin:/usr/sbin',
  }

  class { 'apt':
    debian_url         => '__DEB_URL__',

    # if custom_key_dir is not set, puppet apply fails complaining.
    custom_key_dir     => 'puppet:///modules/site_apt/keys.d',

    # if manage_preferences = true, a preferences.d file will prevent
    # packages from being installed.
    manage_preferences => false,  # if not set, won't install packages
    custom_preferences => false,
  }

  class { 'tails::weblate':
    admins          => 'root',
    db_password     => '123',
    code_git_remote => '__WEBLATE_URL__',
    code_git_revision => 'weblate-2.18',
  }
}
