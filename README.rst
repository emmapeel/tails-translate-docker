Tails Translate Docker image
============================

This directory contains files needed to build a Docker_ image containing
a Debian system with the Tails weblate.pp_ Puppet recipe applied that can be
used for development of the translation platform.

.. _Docker: https://docs.docker.com/
.. _weblate.pp: https://git-tails.immerda.ch/puppet-tails/tree/manifests/weblate.pp

Building
--------

To build the image, you first need to `install Docker
<https://docs.docker.com/install/linux/docker-ce/debian/>`_. You may want to
add your user to the ``docker`` group so you don't need to do things as root.

Then, download the Tails signing key and build the Docker image:

.. code::

  wget https://tails.boum.org/tails-signing.key
  docker build -t tails:translate ./

If you have limited internet connection, you can setup local caches in your
host and pass build arguments to docker builder:

.. code::

  docker build -t tails:translate \
    --build-arg git_url=http://172.17.0.1:8000 \
    --build-arg deb_url=http://172.17.0.1:9999/debian \
    ./

A ``Makefile`` is provided so you can either run ``make`` (or ``make local`` if
you want to use local caches as above) to have the image built with whatever it
needs.

Todo
----

- Fix puppet apply warnings:

  - Warning: Facter: Error loading fact
    /opt/tails/puppet/modules/jenkins/lib/facter/jenkins.rb: cannot load such
    file -- puppet/jenkins/facts                                    

  - Warning: Unknown variable: 'apt::apt_base_dir'. at
    /opt/tails/puppet/modules/apt/manifests/config.pp:69:15
    (also on lines 77 and 78 of the same file. This is responsible for the
    /keys.d directory in the image.)

  - Warning: You cannot collect exported resources without storeconfigs being
    set; the collection will be ignored at
    /opt/tails/puppet/modules/mysql/manifests/server/base.pp:96:5
    (also on lines 97 and 98 of the same file.)

  - Warning: This method is deprecated, please use the stdlib validate_legacy
    function, with Stdlib::Compat::Bool. There is further documentation for
    validate_legacy function in the README. at
    ["/opt/tails/puppet/modules/tails/manifests/backupninja.pp",
    8]:["/opt/tails/puppet/modules/tails/manifests/weblate.pp", 232]
    (at /opt/tails/puppet/modules/stdlib/lib/puppet/functions/deprecation.rb:
    28:in deprecation)

  - Warning: Could not find resource 'Package[mysql-client]' in parameter
    'require' (at
    /opt/tails/puppet/modules/mysql/manifests/server/clientpackage.pp:7)

- Run the weblate website.
