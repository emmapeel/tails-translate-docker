TAG = tails:translate

image: tails-signing.key
	docker build -t $(TAG) ./

# If you are low on bandwidth, you can make copies of git and debian repos
# available on main host.
local: tails-signing.key
	docker build -t $(TAG) \
	  --build-arg git_url=http://172.17.0.1:8000 \
	  --build-arg deb_url=http://172.17.0.1:9999/debian \
	  --build-arg weblate_url=http://172.17.0.1:8000/weblate.git \
	  ./

# from https://tails.boum.org/install/expert/usb/index.en.html
tails-signing.key:
	wget https://tails.boum.org/tails-signing.key
