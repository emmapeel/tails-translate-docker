FROM debian:stretch

ARG git_url=https://git-tails.immerda.ch
ARG deb_url=http://deb.debian.org/debian
ARG weblate_url=https://github.com/WeblateOrg/weblate.git
ENV prefix=/opt/tails/puppet
COPY release_signing_tails.key ${prefix}/modules/site_apt/files/keys.d
COPY apache-vhost.conf /var/lib/weblate/config/apache-vhost.conf

# configure the machine's hostname
RUN echo "translate.lizard" > /etc/hostname
RUN cp /etc/hosts /etc/hosts.new
RUN sed -i 's/^127\.0\.0\.1.*/127.0.0.1 translate.lizard localhost/' /etc/hosts.new
RUN cp -f /etc/hosts.new /etc/hosts

# configure an initial repository and install some utils
RUN echo "deb ${deb_url} stretch main\n deb ${deb_url} stretch-backports main" > /etc/apt/sources.list
RUN apt-get update -qq
RUN apt-get upgrade -qq
RUN apt-get -y install vim man
RUN apt-get -y install puppet
RUN apt-get -y install git

# mysql-server must be installed when weblate.pp is applied, otherwise the
# following error happens:
#
#   Warning: Unknown variable: '::mysql_version'. at
#   /opt/tails/puppet/modules/mysql/manifests/server/cron/backup.pp:16:17
#   Error: Evaluation Error: Error while evaluating a Function Call,
#   'versioncmp' parameter 'a' expects a String value, got Undef at
#   /opt/tails/puppet/modules/mysql/manifests/server/cron/backup.pp:16:6
#   on node 09df71b92f8f
RUN apt-get install -y mysql-server

RUN mkdir -p ${prefix}

# get puppet modules needed for tails::weblate
RUN mkdir -p ${prefix}/modules
RUN for repo in apt tails stdlib vcsrepo mysql user \
                jenkins reprepro gitolite common; \
    do \
        git clone ${git_url}/puppet-${repo}.git ${prefix}/modules/${repo}; \
    done

# Add python dev dependency to prevent:
# siphashc.c:27:20: fatal error: Python.h: No such file or directory
RUN apt-get install -y python-dev

# sudo is needed otherwise the following error happens:
#
#   Error: /Stage[main]/Tails::Weblate/File[/etc/sudoers.d/weblate-admin]/ensure:
#   change from absent to file failed: Could not set 'file' on ensure: No such
#   file or directory @ dir_s_mkdir - /etc/sudoers.d/weblate-admin20180210-7-ni4gec.lock
#   at /opt/tails/puppet/modules/tails/manifests/weblate.pp:87
RUN apt-get install -y sudo

# provide the Tails APT Signing Key for apt
RUN mkdir -p ${prefix}/modules/site_apt/files/keys.d
COPY release_signing_tails.key ${prefix}/modules/site_apt/files/keys.d

ENV PATH=/bin:/usr/bin:/sbin:/usr/sbin

# the node.pp file describes the configuration of the generated image
RUN mkdir -p ${prefix}/manifests
COPY node.pp ${prefix}/manifests/
RUN sed -i "s@__DEB_URL__@${deb_url}@" ${prefix}/manifests/node.pp
RUN sed -i "s@__WEBLATE_URL__@${weblate_url}@" ${prefix}/manifests/node.pp

# hiera is needed for mysql::server::root_password
RUN mkdir -p ${prefix}/hiera
COPY hiera.yaml ${prefix}/
COPY common.yaml ${prefix}/hiera/

RUN puppet apply --debug \
      --modulepath=${prefix}/modules \
      --hiera_config ${prefix}/hiera.yaml \
      ${prefix}/manifests/node.pp
# we add the django settings file, it is private in puppet tails
COPY settings_django_weblate.py /usr/local/share/weblate/weblate/settings.py
# weblate deps
RUN apt-get install -y python-pip python-compressor python-openid
RUN apt-get install -y python-django/stretch-backports lynx
# i dont know how not to install with pip this deps..
RUN pip install social-auth-app-django social-auth-core
RUN pip install djangorestframework==3.7
RUN pip install siphashc
#we make migrations
RUN cd /usr/local/share/weblate/ && ./manage.py migrate
ENTRYPOINT ["/usr/local/share/weblate/manage.py", "runserver"]
# and see the website with lynx at http://127.0.0.1:8000
RUN mkdir -p /var/lib/weblate/repositories/ssh
RUN mkdir -p /var/lib/weblate/repositories/whoosh
RUN mkdir -p /var/lib/weblate/repositories/home
RUN  chown -R weblate:weblate /var/lib/weblate/repositories
